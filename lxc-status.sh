#!/usr/bin/env bash

# INITIALIZATION

LXC_CONTAINERS=($(lxc-ls))


# ERROR CHECKING

if [ 0 == ${#LXC_CONTAINERS[@]} ]; then
    echo -e "No containers to list their status for found!\n"
    exit 1;
fi


# LISTING

echo -e "";

for LXC_CONTAINER in ${LXC_CONTAINERS[@]}
    do
        echo -e "${LXC_CONTAINER}"
        lxc-info -s -n ${LXC_CONTAINER}
        echo -e "";
    done